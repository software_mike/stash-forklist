package com.fernferret.stash.forklist;

import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.project.Project;
import com.atlassian.stash.project.ProjectService;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.util.Page;
import com.atlassian.stash.util.PageImpl;
import com.atlassian.stash.util.PageRequest;
import com.atlassian.stash.util.PageRequestImpl;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AllForksServlet extends HttpServlet {

    private final ProjectService projectService;
    private final RepositoryService repositoryService;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final WebResourceManager webResourceManager;

    private static final Logger log = LoggerFactory.getLogger(AllForksServlet.class);

    public AllForksServlet(ProjectService projectService,
                           RepositoryService repositoryService,
                           SoyTemplateRenderer soyTemplateRenderer,
                           WebResourceManager webResourceManager) {
        this.projectService = projectService;
        this.repositoryService = repositoryService;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.webResourceManager = webResourceManager;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String template;
        Map<String, Object> context = Maps.newHashMap();
        Repository repository;
        String[] path = request.getPathInfo().split("/");
        PageRequest pageRequest = new PageRequestImpl(0, 100);
        Map<String, Page> forkMap = new HashMap<String, Page>();
        if (path.length == 3 && path[1].equals("project") && !path[2].isEmpty()) {
            String projectKey = path[2];
            Project project = this.projectService.getByKey(projectKey);
            if (project == null) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            Page<? extends Repository> forkPage = this.findBaseRepos(projectKey, pageRequest);
            PageRequest localRequest;
            for (Repository repo : forkPage.getValues()) {
                localRequest = new PageRequestImpl(0, 100);
                forkMap.put(repo.getSlug(), this.findForks(repo, localRequest));
            }

            context.put("project", project);
            context.put("repos", forkPage.getValues());
            context.put("forkMap", forkMap);
            // Now build the templates for the view that will show
            this.webResourceManager.requireResourcesForContext("com.fernferret.stash.forklist.project");
            template = "plugin.page.projectForks";
        } else if (path.length == 5 && path[1].equals("project") && !path[2].isEmpty() && !path[4].isEmpty()) {
            String projectKey = path[2];
            String repoKey = path[4];
            repository = this.repositoryService.getBySlug(projectKey, repoKey);
            if (repository == null) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            Page<Repository> forkPage = findForks(repository, pageRequest);

            context.put("forkPage", forkPage);
            // The base repo.
            context.put("repository", repository);
            this.webResourceManager.requireResourcesForContext("com.fernferret.stash.forklist.repo");
            template = "plugin.page.repoForks";
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        response.setContentType("text/html; charset=UTF-8");
        try {
            this.soyTemplateRenderer.render(
                    response.getWriter(),
                    "com.fernferret.stash.forklist:server-side-soy",
                    template, context);
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            } else {
                throw new ServletException(e);
            }
        }
    }

    /**
     * Find all direct forks of the {@link com.atlassian.stash.repository.Repository} given.
     * This is NOT recursive.
     *
     * @param origin Repository to search for forks.
     * @param pageRequest Pagination request (max/min to return).
     * @return Pages of Repositories.
     */
    private Page<Repository> findForks(Repository origin, PageRequest pageRequest) {
        List<Repository> values = Lists.newLinkedList();
        boolean isLastPage = false;
        PageRequest tmpPageRequest = new PageRequestImpl(0, 10);
        while (tmpPageRequest != null && values.size() < pageRequest.getLimit() && !isLastPage) {
            Page<? extends Repository> forkPage = this.repositoryService.findByOrigin(origin, pageRequest);
            if (forkPage.getIsLastPage()) {
                isLastPage = true;
            }
            for (Repository repo : forkPage.getValues()) {
                values.add(repo);
            }
            tmpPageRequest = forkPage.getNextPageRequest();
        }
        return new PageImpl<Repository>(pageRequest, values, isLastPage);
    }

    /**
     * Returns the repositories that belong to a particular project.
     *
     * @param projectKey  Unique @see org.java.lang.String
     * @param pageRequest Pagination request (max/min to return).
     * @return Pages of Repositories.
     */
    private Page<Repository> findBaseRepos(String projectKey, PageRequest pageRequest) {
        List<Repository> values = Lists.newLinkedList();
        boolean isLastPage = false;
        PageRequest tmpPageRequest = new PageRequestImpl(0, 10);
        while (tmpPageRequest != null && values.size() < pageRequest.getLimit() && !isLastPage) {
            Page<? extends Repository> forkPage = this.repositoryService.findByProjectKey(projectKey, pageRequest);
            if (forkPage.getIsLastPage()) {
                isLastPage = true;
            }
            for (Repository repo : forkPage.getValues()) {
                // Only add non-forks here. This is we don't see repos that were forked to this project
                if (!repo.isFork() && repo.isForkable()) {
                    values.add(repo);
                }
            }
            tmpPageRequest = forkPage.getNextPageRequest();
        }

        return new PageImpl<Repository>(pageRequest, values, isLastPage);
    }
}